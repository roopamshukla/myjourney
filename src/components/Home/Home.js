import React, { useState } from 'react';
import styles from './Home.module.css';

const Home = () => {

  const [from, setFrom] = useState("");
  const [to, setTo] = useState("");

  const fromArray = [
    "from1",
    "from2"
  ]

  const toArray = [
    "to1",
    "to2"
  ]

  const para = [{
    from: "from1",
    to: "to1",
    data: ["lalallallalaa", "lallallla", "blahhhh"]
  },
  {
    from: "from2",
    to: "to1",
    data: ["qqqqqqqqqqqqqqqqq", "lallallqqqqqqqqla", "qqqqqqqblahhhh"]
  },
  {
    from: "from1",
    to: "to2",
    data: ["bbbbbbbbbbbbb", "bbbbbbbbbbb", "qqqqbbbbbbbbbbbbqqqblahhhh"]
  },
  {
    from: "from2",
    to: "to2",
    data: ["pppppppppppp", "[[[[[[[[[[[[bbbbbbbbbbb]]]]]]]]]]]]", "nnnnnnnnnnnnn"]
  }]

  let handleFrom = (check, fromCheck) => {
    if (check) {
      setFrom(fromCheck)
    }
  }


  let handleTo = (check, toCheck) => {
    if (check) {
      setTo(toCheck)
    }
  }

  let [toDisplay] = para.filter(data => data.from === from && data.to === to);

  return (
    <div className={styles.Home} >
      <h1>My Journey</h1>
      From :
      {fromArray.map(function (from) {
        return <div key={from}><input type="radio" name="from" onChange={e => handleFrom(e.target.checked, from)} />{from}</div>
      })}
      <br />
      To :
      {toArray.map(function (to) {
        return <div key={to}><input type="radio" name="to" onChange={e => handleTo(e.target.checked, to)} />{to}</div>
      })}
      <br />
      {toDisplay?.data ? (toDisplay.data.map(function (point) {
        return <div key={point}><br /> &#9655; {"   " + point}</div>
      })) : "Please select values"}
    </div>
  )
};

Home.propTypes = {};

Home.defaultProps = {};

export default Home;
