import './App.css';
import Home from "./components/Home/Home"
import NET from 'vanta/dist/vanta.net.min'
import * as THREE from 'three'

import React, { useState, useEffect, useRef } from 'react';

function App() {
  const [vantaEffect, setVantaEffect] = useState(0)


  const myRef = useRef(null)
  useEffect(() => {
    if (!vantaEffect) {
      setVantaEffect(NET({
        el: myRef.current,
        THREE: THREE
      }))
    }
    return () => {
      if (vantaEffect) vantaEffect.destroy()
    }
  }, [vantaEffect])

  return (
    <div className="App" ref={myRef}>
      <Home />
    </div>
  );
}

export default App;
